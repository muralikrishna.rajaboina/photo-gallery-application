import './App.css';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import { Component } from 'react';
import { Amplify ,Auth} from 'aws-amplify'
import config from './aws-exports'
import '@aws-amplify/ui-react/styles.css'
import { withAuthenticator, AmplifyProvider } from '@aws-amplify/ui-react';
import PhotoContainer from "./PhotoContainer"
import User from './user'
Amplify.configure(config)

class App extends Component {
  constructor() {
    super();
    this.state = {
      photos: []
    };
  }
  signOut = async () => {
    await Auth.signOut()

  }
 
  // Then in your render method.
  componentDidMount() {
    fetch('https://d6k01ir062.execute-api.ap-southeast-1.amazonaws.com/dev/photos').then(response => {
      console.log('response', response);
      if (!response.ok) {
        throw Error("Error fetching the photos API")
      }
      return response.json()
        .then(allData => {
          this.setState({ photos: allData });
        })
        .catch(err => {
          throw Error(err.message);
        })
    })
  }
  
  render() {
    return (
      <AmplifyProvider>
        <section class="form-control form-control-lg" className='app'>
        <button onClick={this.signOut} className="signOutButton">SignOut</button>
        <User/>
        <PhotoContainer photos={this.state.photos} />
      </section>
      // </AmplifyProvider>
    );
  }
}

export default withAuthenticator(App)
