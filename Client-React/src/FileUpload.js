import React, { useState } from 'react'
import axios from 'axios';

function FileUpload() {

    const [file, setFile] = useState();
    const [fileName, setFileName] = useState("");
    const saveFile = (e) => {
        setFile(e.target.files[0]);
        setFileName(e.target.files[0].name);
    };

    const uploadFile = async (e) => {
        const formData = new FormData();
        formData.append("file", file);
        formData.append("fileName", fileName);
        try {
            const res = await axios.post(
                "https://d6k01ir062.execute-api.ap-southeast-1.amazonaws.com/dev/photo",
                formData
            );
            console.log(res);
        } catch (ex) {
            console.log(ex);
        }

    };
    const refreshPage = async () => {
        window.location.reload(true);
    }

    return (
        <div className="App">
            <div class="form-control form-control-lg">
                <input id="formFileLg" type="file" onChange={saveFile} />
                {/* <input type="file" onChange={saveFile} /> */}
                <button onClick={() => {
                    uploadFile();
                    refreshPage()
                }}>Upload</button>
            </div>
        </div>
    );

}

export default FileUpload;
