
const Photo = (props, i) => {
    return (
        <section>
            <div key={i} class="col">
                <div class="card">
                    <div class="bg-image hover-overlay ripple" data-mdb-ripple-color="light">
                        <img src={props.photoUrl} alt={props.name} class="img-fluid" />
                        <a href="#!">
                            <div class="mask" style={{ 'background-color': 'rgba(251, 251, 251, 0.15)' }}></div>
                        </a>
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">{props.name}</h5>
                    </div>
                </div>
            </div>
        </section>
    )
}


export default Photo