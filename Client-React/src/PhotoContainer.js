import React from 'react'
import Photo from "./Photo"
const PhotoContainer = props => {
    const displayPhotos = () => {
        return props.photos.map(photo => {
            return <Photo photoUrl={photo.photoUrl} name={photo.name} id={photo.id}></Photo>
        })
    }
    return (

        <section >
            <div class="row row-cols-1 row-cols-md-3 g-4">
                {displayPhotos()}
            </div>
        </section>

    )
}

export default PhotoContainer