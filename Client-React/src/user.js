import { useState, useEffect } from 'react';
import { Auth } from 'aws-amplify'
import FileUpload from './FileUpload'


const User = () => {
    const [user, setUser] = useState();
    useEffect(() => {
        prepareUser()
    }, [])

    const prepareUser = async () => {
        Auth.currentAuthenticatedUser({
            bypassCache: false  // Optional, By default is false. If set to true, this call will send a request to Cognito to get the latest user data
        }).then(user => {
            setUser(user.username)
        })
            .catch(err => {
                // console.log(err)
            });
    }
    if ({ user } != 'b4c98643-11fa-48a0-b51a-5d4759cb23aa') {

        return (
            <div><h3>Welcome To Photo Gallery App</h3></div>
        )
    }
    else {
        <section>
            <h3>Welcome To Photo Gallery App</h3>
            <FileUpload />
        </section>
    }

}


export default User