const AWS = require("aws-sdk");
const dynamoDb = new AWS.DynamoDB.DocumentClient();
const uuid = require("uuid");
var express = require('express')
const fileUpload = require('express-fileupload');
const app = express();
app.use(fileUpload());

module.exports.getAllPhotos = () => {
  const params = {
    TableName: process.env.DYNAMODB_TABLE
  };

  return dynamoDb.scan(params).promise();
};

module.exports.getPhotoById = id => {
  const params = {
    TableName: process.env.DYNAMODB_TABLE,
    Key: {
      id: id
    }
  };

  return dynamoDb.get(params).promise();
};
module.exports.uploadPhoto= (fileName,Content)=>{

    const params = {
        Bucket: 'photo-gallery-app-serverlessdeploymentbucket-ix30m6hemn6w',
        Key: fileName, // File name you want to save as in S3
        Body: Content
    };
  
    const s3 = new AWS.S3();
    return s3.upload(params).promise();
};


module.exports.createPhoto = (name, photoUrl) => {

  const timestamp = new Date().getTime();

  const params = {
    TableName: process.env.DYNAMODB_TABLE,
    Item: {
      id: uuid.v1(),
      name: name,
      photoUrl: photoUrl,
      createdAt: timestamp,
      updatedAt: timestamp
    }
  };

  return dynamoDb.put(params).promise().then(() => params.Item)
}

module.exports.deletePhoto = (id) => {

  const params = {
    TableName: process.env.DYNAMODB_TABLE,
    Key: {
      id: id,
    },
  }

  return dynamoDb.delete(params).promise();
}