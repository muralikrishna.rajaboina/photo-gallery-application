const databaseAccess = require("../databaseAccess")
const response = require("../response");
const validator = require("../validator");
const AWS = require("aws-sdk");
const parser = require('lambda-multipart-parser');

module.exports.handler = async (event, context, callback) => {

    let error = [];
    const result = await parser.parse(event);
    console.log(result);

  
    const fileName =result.files[0].filename;
    const Content =result.files[0].content;
    console.log(Content);
    // console.log(data);
    try {
        let imageDetails = await databaseAccess.uploadPhoto(fileName, Content);
        const name = imageDetails.key;
        const photoUrl = imageDetails.Location;
        if (!validator.validate(name)) {
            console.log("name is null or empty");
            error.push({ name: "name is null or empty" });
        }

        if (!validator.validate(photoUrl)) {
            console.log("photoUrl is null or empty");
            error.push({ photoUrl: "photoUrl is null or empty" });
        }

        if (error.length > 0) {
            callback(null, response.buildResponse(400, { error: error }));
            return;
        }
        try {
            let result = await databaseAccess.createPhoto(name, photoUrl);
            callback(null, response.buildResponse(200, result));
        }
        catch (error) {
            console.log(error);
            callback(null, response.buildResponse(501, { "message": "Couldn't create the Photo item.", "error": error }));
        }
    }
    catch (error) {
        console.log(error);
        callback(null, response.buildResponse(501, { "message": "Couldn't upload the Photo item.", "error": error }));

    }
};
