const databaseAccess = require("../databaseAccess")
const response = require("../response");

module.exports.handler = async (event, context, callback) => {

    try {
        await databaseAccess.deletePhoto(event.pathParameters.id);
        callback(null, response.buildResponse(204));
    } catch (err) {
        callback(null, response.buildResponse(501, { message: "Couldn't delete the Photo item.", error: err }));
    }
};
